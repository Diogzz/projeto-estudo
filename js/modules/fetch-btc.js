export default function initFetchBtc() {
    async function fetchBtc(url) {
        const bitcoins = await fetch(url);
        const bitcoisJson = await bitcoins.json();
        const doeBiticoin = document.querySelector(".doe-biticoin");
    
        doeBiticoin.innerText = ( 100 / bitcoisJson.BRL.buy).toFixed(4);
    }
    
    fetchBtc ("https://blockchain.info/ticker");
}