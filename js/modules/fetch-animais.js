
import initAnimaNumeros from './anima-numeros.js';

export default function initFetchAnimais() {
    async function fetchAnimais(url) {
        const animais = await fetch(url);
        const animaisJson = await animais.json();
        const containerAnimais = document.querySelector(".numeros-grid");
    
        animaisJson.forEach(animal => {
            containerAnimais.appendChild(criaDivAnimais(animal));
        });

        initAnimaNumeros();
    }
    
    fetchAnimais ("../api/animais.json");
    
    function criaDivAnimais(animal) {
        const div = document.createElement('div');
        div.classList.add('numero-animal');
        div.innerHTML = `<h3>${animal.especie}</h3>
                         <span data-numero>${animal.quantidade}</span>
                        `
        return div;
    }
}