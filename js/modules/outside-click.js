
// elementoClicadoThis é a mesma coisa que 'this' passado junto a função callback;
export default function outsideClick(elementoClicadoThis, callback) {
    const html = document.documentElement;
    const outside = 'data-outsie';

    if(!elementoClicadoThis.hasAttribute(outside)) {
       setTimeout(() => html.addEventListener('click', handleOutSideClick));
        elementoClicadoThis.setAttribute(outside, '')
    }

    function handleOutSideClick(evento) {
        if (!elementoClicadoThis.contains(evento.target)) {
            html.removeEventListener('click', handleOutSideClick);
            elementoClicadoThis.removeAttribute(outside);
            callback();
        }
    }
}